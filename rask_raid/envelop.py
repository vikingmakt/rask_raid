from msg import MSG
from rask.base import Base
from rask.parser.utcode import UTCode

__all__ = ['Envelop']

class Envelop(Base):
    def __init__(self):
        self.utcode = UTCode()

    def pack(self,msg,future):
        try:
            assert msg.valid
        except AssertionError:
            future.set_result(False)
        except:
            raise
        else:
            def on_encode(_):
                future.set_result(_.result())
                return True

            self.utcode.encode(
                msg.payload,
                future=self.ioengine.future(on_encode)
            )
        return True
        
    def unpack(self,msg,future):
        def on_decode(_):
            future.set_result(MSG(payload=_.result()))
            return True
        
        self.utcode.decode(
            msg,
            future=self.ioengine.future(on_decode)
        )
        return True
